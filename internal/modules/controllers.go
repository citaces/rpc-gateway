package modules

import (
	"gitlab.com/citaces/rpc-gateway/internal/infrastructure/component"
	acontroller "gitlab.com/citaces/rpc-gateway/internal/modules/auth/controller"
	aservice "gitlab.com/citaces/rpc-gateway/internal/modules/auth/service"
	ucontroller "gitlab.com/citaces/rpc-gateway/internal/modules/user/controller"
	"gitlab.com/citaces/rpc-gateway/internal/modules/user/service"
)

type Controllers struct {
	Auth acontroller.Auther
	User ucontroller.Userer
}

func NewControllers(auth *aservice.AuthServiceJSONRPC, users *service.UserServiceJSONRPC, components *component.Components) *Controllers {

	authController := acontroller.NewAuth(auth, components)
	userController := ucontroller.NewUser(users, components)

	return &Controllers{
		Auth: authController,
		User: userController,
	}
}
